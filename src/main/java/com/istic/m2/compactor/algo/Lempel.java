package com.istic.m2.compactor.algo;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/***
 * Implementation of the Lempel algorithm:
 * TODO: Convert methods to use Input and OutputStreams
 * FIXME: Issues with decoding (especially)
 * @author blegarrec
 */
public class Lempel implements Algo {
/*
	@Override
	public byte[] compress(byte[] file) {

		Map<String, Integer> dictionary = new HashMap<>();
		List<Character> indexValues = new ArrayList<>();

		String previous = String.valueOf((char) file[0]);
		String current = String.valueOf((char) file[1]);
		String tmp = "";

		dictionary.put(previous, 1);

		Integer value = 1;

		// adds all characters of the input to the dictionary
		for (int i = 1; i < file.length; i++) {
			String test = String.valueOf((char) file[i]);
			if (!dictionary.containsKey(test)) {
				value++;
				dictionary.put(test, value);
			}
		}

		// algo
		for (int i = 1; i < file.length; i++) {
			current = String.valueOf((char) file[i]);
			tmp = previous + current;
			if (dictionary.containsKey(tmp)) {
				if (tmp.length() < 5) {
					previous = tmp;
				} /*else if(dictionary.containsKey(tmp)){
					value++;
					dictionary.put(tmp, value);
					indexValues.add(new Character((char) (dictionary.get(previous).intValue())));
					previous = current;
				}*//*else {
					previous = current;
				}

			} else {
				value++;
				dictionary.put(tmp, value);
				indexValues.add(new Character((char) (dictionary.get(previous).intValue())));
				previous = current;
			}
		}

		List<Byte> bytes = convertDictionaryToByte(dictionary);
		for (Character c : indexValues) {
			String tmpchar = Character.toString(c);
			bytes.add(tmpchar.getBytes()[0]);
		}

		byte[] res = new byte[bytes.size()];
		for (int i = 0; i < bytes.size(); i++) {
			// peut être problème ici mais pas sur
			res[i] = bytes.get(i);
		}

		return res;
	}
	*/

	/***
	 * 
	 * @param dictionary
	 * @return
	 */
	public List<Byte> convertDictionaryToByte(Map<String, Integer> dictionary) {
		List<Byte> res = new ArrayList<>();

		for (Map.Entry<String, Integer> entry : dictionary.entrySet()) {

			int pos = 0;
			String binaryString = entry.getKey();
			res.add((byte) (binaryString.length()));
			for (int i = 0; i<binaryString.length();i++) {
				res.add((byte) binaryString.charAt(i));
			}
			/*while (pos < binaryString.length()) {
				byte nextByte = 0x00;
				for (int i = 0; i < 8 && pos + i < binaryString.length(); i++) {
					nextByte = (byte) (((int) nextByte) << 1);
					nextByte += binaryString.charAt(pos + i) == '0' ? 0x0 : 0x1;
				}
				res.add(nextByte);
				pos += 8;

			}*/
			Integer index = entry.getValue();
			if (index < 256) {
				res.add((byte) 0);
				res.add(index.byteValue());
			} else {
				res.add((byte) ((index >> 8) & 0xFF));
				res.add((byte) (index & 0xFF));
			}
			// res.add(entry.getValue().byteValue());
		}
		Integer size = res.size();
		// res.add(0, (byte) size);
		if (size < 256) {
			res.add(0, size.byteValue());
			res.add(0, (byte) 0);
		} else {
			res.add(0, (byte) (size & 0xFF));
			res.add(0, (byte) ((size >> 8) & 0xFF));
		}
		return res;
	}

	/*
	@Override
	public byte[] expand(byte[] file) {

		int size = ((file[0] & 0xff) << 8) | (file[1] & 0xff);
		
		byte[] dictionaryBinary = Arrays.copyOfRange(file, 2, size + 2);
		byte[] contentBinary = Arrays.copyOfRange(file, size + 2, file.length);
		Map<String, Integer> dictionary = decodeDictionary(dictionaryBinary);

		return decodeContent(contentBinary, dictionary);
	}*/

	/***
	 * 
	 * @param size
	 * @param file
	 * @return
	 */
	public Map<String, Integer> decodeDictionary(byte[] file) {
		Map<String, Integer> dictionary = new HashMap<>();

		for (int i = 0; i < file.length; i++) {
			int codesize = file[i];
			i++;
			
			String key = "";
			for (int j = i; j<i+codesize;j++) {
				key += (char) file[j];
			}
			i+= codesize;
			
			int value = (file[i] << 8) + file[i + 1];
			dictionary.put(key, value);
			i++;
		}

		return dictionary;
	}

	/***
	 * 
	 * @param file
	 * @param dictionary
	 * @return
	 */
	public byte[] decodeContent(byte[] file, Map<String, Integer> dictionary) {

		return null;
	}

	@Override
	public int compress(InputStream input, OutputStream output) {
		return 0;
	}

	@Override
	public int expand(InputStream input, OutputStream output) {
		return 0;
	}

}
