package com.istic.m2.compactor.algo;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;

/***
 * there is no unsigned type in java so use char
 * @author vantrop
 *
 */
public class Huffman implements Algo {

	// Huffman tree node
	protected static class Node {
		private final char value;
		private final int freq;
		private Node left, right;

		Node(char value, int freq, Node left, Node right) {
			this.value = value;
			this.freq = freq;
			this.left = left;
			this.right = right;
		}

		Node(char ch, int freq) {
			this.value = ch;
			this.freq = freq;
		}
	}

	private int[] freq = new int[256];

	@Override
	public int compress(InputStream file, OutputStream out) {
		Node root;

		try {
			root = buildTree(file);
		} catch (IOException e) {
			return 3;
		}

		Map<Character,String> codeTable = new HashMap<>();
		encode(root,"",codeTable);
		
		codeTable.values().stream().forEach((value)->{
			if (value.length()>24) {
				System.err.println(value.length());
			}
		});
		
		try {
			if (file instanceof ByteArrayInputStream) {
				file.reset();
			} else {
				((FileInputStream) file).getChannel().position(0);
			}
		} catch (IOException e) {
			System.err.println(3);
			return 3;
		}
		return convertoBytes(codeTable, file, out);
	}
	
	/***
	 * provide the code table that has to be embedded in the file as a header
	 * in format first byte size of the table(nb of code)
	 * each key value as byte1 char, byte2 code size, byte3 code
	 * @param codeTable
	 * @return compressed file header 
	 */
	public List<Byte> convertCodeTableToByte(Map<Character,String> codeTable) {
		List<Byte> res = new ArrayList<>();
		
		for(Map.Entry<Character, String> entry : codeTable.entrySet()) {
			//String tmpchar = Character.toString(entry.getKey());
			res.add((byte)((char) entry.getKey()));
			res.add((byte)((entry.getValue().length())));
			int pos = 0;
			String binaryString = entry.getValue();
			while(pos < binaryString.length()){
		        byte nextByte = 0x00;
		        for(int i=0;i<8 && pos+i < binaryString.length(); i++){
		            nextByte = (byte)(((int)nextByte) << 1);
		            nextByte += binaryString.charAt(pos+i)=='0'?0x0:0x1;
		        }
		        res.add(nextByte);
		        pos+=8;
		    }
			
		
		}
		int size = res.size();
		res.add(0,(byte)(size%3));
		res.add(0,(byte)(size/3));
		return res;
	}
	
	/***
	 * convert each byte of the file using the codeTable
	 * @param codeTable
	 * @param file
	 * @return compressed file content as byte array
	 */
	private int convertoBytes(Map<Character,String> codeTable, InputStream file, OutputStream out) {
		List<Byte> bytes = new ArrayList<>();
		bytes.addAll(convertCodeTableToByte(codeTable));
		// +2 for size*3 and size%3
		// +2 for max size of size%3
		if (bytes.size() > 256*3 +2 +2) {
			return 5;
		}
		try {
			for(Byte b : bytes) {
				out.write(b);
			}
			out.flush();
			bytes = new ArrayList<>();
			
			String binaryString = "";
			while(file.available()>0) {
				char byt = (char)file.read();
				boolean isLast = file.available() == 0;
				binaryString += convertoBinaryString(codeTable, byt);
				if (isLast) {
					int size = 0;
					if (binaryString.length()%8 != 0) {
						size = 8-binaryString.length()%8;
						for (int k = 0;k<size;k++) {
							binaryString += "0";
						}
					}
					int pos = 0;
				    while(pos < binaryString.length()){
				        byte nextByte = 0x00;
				        for(int i=0;i<8 && pos+i < binaryString.length(); i++){
				            nextByte = (byte)(((int)nextByte) << 1);
				            nextByte += binaryString.charAt(pos+i)=='0'?0x0:0x1;
				        }
				        out.write(nextByte);
				        pos+=8;
				    }
				    out.write(size);
				} else {
					if (binaryString.length()>8) {
						byte nextByte = 0x00;
						int pos = 0;
				        for(int i=0;i<8 && pos+i < binaryString.length(); i++){
				            nextByte = (byte)(((int)nextByte) << 1);
				            nextByte += binaryString.charAt(pos+i)=='0'?0x0:0x1;
				        }
				        out.write(nextByte);
				        binaryString = binaryString.substring(8);
					}
				}
				
			}
		} catch(IOException e) {
			return 3;
		}
		return 0;
	}
	
	/***
	 * convert each byte of a file to a String using the codeTable
	 * and concatenate each such acquired code in the result String
	 * @param codeTable
	 * @param file the file to convert
	 * @return final String binary value of the file (String of 0 and 1 like 00101010001000110)
	 */
	private String convertoBinaryString(Map<Character,String> codeTable, byte[] file) {
		String res = "";
		//converting
		for(byte b : file) {
			res += codeTable.get((char)b);
		}
		return res;
	}
	
	private String convertoBinaryString(Map<Character,String> codeTable, char byt) {
		String res = codeTable.get((char)byt);
		return res;
	}
	
	/***
	 * builds recursively the code table 
	 * @param root the parent node
	 * @param code of the parent
	 * @param codeTable
	 */
	public static void encode(Node root, String code, Map<Character, String> codeTable) {
		if (root == null) {
			return;
		}

		if (root.left == null && root.right == null) {
			codeTable.put(root.value, code);
		}

		encode(root.left, code + "0", codeTable);
		encode(root.right, code + "1", codeTable);

	}
	
	/***
	 * build the huffman tree
	 * @param file
	 * @return the root node
	 * @throws IOException 
	 */
	public Node buildTree(InputStream file) throws IOException {
		freq = new int[256*256];
		Set<Character> presentOnes = new HashSet<>();
		int i;
		while(( i =file.read()) != -1) {
			freq[(char)i]++;
			presentOnes.add((char)i);
		}
		PriorityQueue<Node> pq = new PriorityQueue<>((l, r) -> l.freq - r.freq);
		for (Character b : presentOnes) {
			Node nNode = new Node(b, freq[b]);
			pq.add(nNode);
		}
		while (pq.size() > 1) {
			Node left = pq.poll();
			Node right = pq.poll();
			int sum = left.freq + right.freq;
			pq.add(new Node('\0', sum, left, right));
		}

		return pq.peek();
	}

	@Override
	public int expand(InputStream file, OutputStream out) {
		try {
			Map<Character,String> codeTable = decodeCodeTable(file);
			if (file instanceof ByteArrayInputStream) {
				file.reset();
			} else {
				((FileInputStream) file).getChannel().position(0);
			}
			int first = file.read();
			int second = file.read();
			file.skip(first *3 + second);
			
			//byte[] content = Arrays.copyOfRange(file, limit + 1, file.length);
			
			decodeContent(codeTable,file,out);
		} catch (IOException e) {
			return 3;
		}
		return 0;
	}
	
	/***
	 * decode the code table at the begining of the compressed file
	 * @param file the complete file
	 * @return the code table as a map<char,code>
	 * @throws IOException 
	 */
	public Map<Character,String> decodeCodeTable(InputStream file) throws IOException {
		Map<Character,String> res =new HashMap<>();
		
		int size = file.read();
		size *= 3;
		size += file.read();
		
		for(int i=0;i<size;i+=3) {
			
			char c = (char)file.read();
			int codeSize = file.read();
			String pre = "";
			byte code = 0;
			int codeSizeEdit = codeSize;
			int nbtour =0;
			while (codeSizeEdit > 8) {
				pre +=byteCodeToString((byte)file.read());
				codeSizeEdit-=8;
				nbtour++;
				i++;
			}
			code = (byte)file.read();
			
			String codeS = byteCodeToString(code);
			if (codeSize>8) {
				codeS = codeS.substring(codeS.length()-(codeSize-nbtour*8), codeS.length());
			} else {
				codeS = codeS.substring(codeS.length()-codeSize, codeS.length());
			}
			if (pre != "") {
				codeS = pre + codeS;
			}
			res.put(c, codeS);
		}
		
		
		return res;
	}
	
	/***
	 * convert byte binary value to string ex: a byte representing 3 will output 00000011
	 * @param octet the byte to convert
	 */
	private String byteCodeToString(byte octet) {
		String res = "";
		for(int i=7;i>=0;i--) { // in reverse to output in the correct order
			res += String.valueOf(getBit(octet,i));
		}
		return res;
	}
	/***
	 * output as an int the value of a bit at postion k in the byte n
	 * @param n the byte
	 * @param k the position of the requested byte
	 * @return
	 */
	private int getBit(byte n, int k) {
	    return (n >> k) & 1;
	}
	
	/***
	 * decode the content of a compressed file (using huffman)
	 * @param codeTable code table of the file
	 * @param content the content part of the file
	 * @return the value to output as byte array
	 */
	private int decodeContent(Map<Character,String> codeTable ,InputStream file, OutputStream out) throws IOException {
		String res ="";
		//revert the map to ease the conversion
		Map<String, Character> swapped = codeTable.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
		List<String> orderedCodes = new ArrayList<>(swapped.keySet());
		orderedCodes.sort(new Comparator<String>() {
			@Override
			public int compare(String arg0, String arg1) {
				return arg0.length() - arg1.length();
			}
		});
		
		
		String binaryString = "";
		
		//int index = 0;
		while(file.available()>0) {
			byte byt = (byte)file.read();
			boolean isLast = file.available() == 1;
			binaryString += byteCodeToString(byt);
			if (isLast) {
				byte numberToSkip = (byte) file.read();
				binaryString = binaryString.substring(0, binaryString.length()-numberToSkip);				
			}
			String currentValue = "";
			for (int index=0;index < binaryString.length();index++) {
				currentValue += binaryString.charAt(index);
				
				for(String code : orderedCodes) {
					if((code.length()==currentValue.length())) {
						if(code.charAt(currentValue.length()-1) == currentValue.charAt(currentValue.length()-1)) {
							if(code.equals(currentValue)) {
								res += swapped.get(code);
								currentValue="";
								binaryString = binaryString.substring(code.length());
								index = -1;//passe a 0 à la fin de la boucle
								break;
							}
						}
					}
				}
			}
			char[] characters = res.toCharArray();
			res = "";
			for (char c : characters) {
				out.write((byte)c);
			}
			out.flush();
			
		}
		/*
		String binaryString="";
		for(byte octet : content) {
			binaryString += byteCodeToString(octet);
		}
		//substring the total number to skip
		binaryString = binaryString.substring(0, binaryString.length()-numberToSkip);
		
		String currentValue="";
		int index=0;
		while(binaryString.length()>index) {
			currentValue += binaryString.charAt(index);
			index++;
			
			for(String code : orderedCodes) {
				if((code.length()==currentValue.length())) {
					if(code.charAt(currentValue.length()-1) == currentValue.charAt(currentValue.length()-1)) {
						if(code.equals(currentValue)) {
							res += swapped.get(code);
							currentValue="";
							break;
						}
					}
				}
			}
		}
		
		char[] characters = res.toCharArray();
		byte[] resbytes = new byte[characters.length];
		for (int i=0;i<characters.length;i++) {
			resbytes[i] = (byte) characters[i];
		}*/
		
		return 0;
	}
	
	
	
	

}
