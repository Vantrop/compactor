package com.istic.m2.compactor.algo;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

public interface Algo {
	
	
	public int compress(InputStream input, OutputStream output);
	
	public int expand(InputStream input, OutputStream output);
	

}
