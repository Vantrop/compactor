package com.istic.m2.compactor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;

import com.istic.m2.compactor.algo.Algo;
import com.istic.m2.compactor.algo.Huffman;
import com.istic.m2.compactor.algo.Lempel;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

@Command(name = "compactor", mixinStandardHelpOptions = true, version = "compactor v0.1",
description = "compress or defalate files using various algorithms")
public class App implements Callable<Integer>{
	
	@Parameters(index = "0", description = "The file to compress or defalate")
    private File inputFile;
	
	@Parameters(index = "1", description = "The output file")
	private File outputFile;
	
	@Option(names = "-d", description = "create a new decompressed file")
    private boolean decompress;
	
	@Option(names = "-c", description = "create a new compressed file")
    private boolean compress;
	
	
	@Option(names = {"-a", "--algorithm"}, description = "Huffman, LZW")
    private String algorithm = "Huffman";
	
	private Algo algo = null;
	
    public static void main( String[] args ) {
    	int exitCode = new CommandLine(new App()).execute(args);
    	System.exit(exitCode);
    }

	@Override
	public Integer call() throws Exception {
		InputStream fileContents = new FileInputStream(inputFile);
		
		OutputStream result = new FileOutputStream(outputFile);
		
		switch(algorithm) {
		case "LZW": algo = new Lempel();
			break;
		case "Huffman": algo = new Huffman();
			break;
		default:
			fileContents.close();
			result.close();
			return 1;
		}
		if (compress && decompress) {
			fileContents.close();
			result.close();
			return 2;
		} else if (decompress) {
			return algo.expand(fileContents, result);
		} else {
			return algo.compress(fileContents, result);
		}
	}

	public Algo getAlgo() {
		return algo;
	}

	
	
}
