package com.istic.m2.compactor.util;

import java.util.Random;

/***
 * Utility class that contains random generation methods for different types.
 */
public class RandomGenerator {

	private static int min = 2;
	private static int max = 1000;
	
	/**
	 * @return a random String of a size between min and max
	 */
	public static String generateRandomString() {
		byte[] array = new byte[generateRandomIntInRange()];
		new Random(System.currentTimeMillis()).nextBytes(array);
		String generatedString = new String(array);
		
		return generatedString;
	}
	
	/**
	 * @return a random alphanumeric String of a size between min and max
	 */
	public static String generateRandomAlphanumericString() {
		int leftLimit = 48; // numeral '0'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = generateRandomIntInRange();
	    Random random = new Random(System.currentTimeMillis());
	 
	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
	 
	    return generatedString;
	}
	
	/**
	 * @return a random int between min and max
	 */
	public static int generateRandomIntInRange() {
		return new Random(System.currentTimeMillis()).nextInt((max-min) +1) +min;
	}
	
}
