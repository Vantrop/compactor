package com.istic.m2.compactor;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.istic.m2.compactor.algo.Huffman;
import com.istic.m2.compactor.algo.Lempel;

import picocli.CommandLine;

/**
 * Unit test for simple App.
 */
public class AppTest {
    
	ByteArrayOutputStream err = new ByteArrayOutputStream(); 
	
	@BeforeEach
    public void init()
    {
        System.setErr( new PrintStream(err));
    }
	
	@Test
    public void testNoArgIncorect()
    {
		String[] args = {"sample/input1", "sample/output1"}; 
		int exitCode = new CommandLine(new App()).execute(args);
		try {
			err.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String resultErr = new String (err.toByteArray());
		assertTrue(resultErr.isEmpty());
		assertSame(0, exitCode);
    }
	
	@Test
	public void testAlgoIncorrect() {
		String[] args = {"-a", "" ,"sample/input1", "sample/output1"}; 
		int exitCode = new CommandLine(new App()).execute(args);
		assertSame(1, exitCode);
	}
	
	@Test void testCompressAndDecompressIncorrect() {
		String[] args = {"-c", "-d" ,"sample/input1", "sample/output1"}; 
		int exitCode = new CommandLine(new App()).execute(args);
		assertSame(2, exitCode);
	}
	
	@Test
	public void testAlgoLempel() {
		String[] args = {"-a", "LZW" ,"sample/input1", "sample/output1"}; 
		App app = new App();
		int exitCode = new CommandLine(app).execute(args);
		assertSame(0, exitCode);
		assertTrue(app.getAlgo() instanceof Lempel);
	}
	
	@Test
	public void testAlgoHuffman() {
		String[] args = {"-a", "Huffman" ,"sample/input1", "sample/output1"}; 
		App app = new App();
		int exitCode = new CommandLine(app).execute(args);
		assertSame(0, exitCode);
		assertTrue(app.getAlgo() instanceof Huffman);
	}
	
	@Test
	public void testAlgoDefault() {
		String[] args = {"sample/input1", "sample/output1"}; 
		App app = new App();
		int exitCode = new CommandLine(app).execute(args);
		assertSame(0, exitCode);
		assertTrue(app.getAlgo() instanceof Huffman);
	}
	
}
