package com.istic.m2.compactor.algo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LempelTest {

	private Lempel lempel;

	@BeforeEach
	public void setup() {
		lempel = new Lempel();
	}

	@Test
	public void isDictionarySizeOK() {
		Map<String, Integer> dictionary = new HashMap<>();
		dictionary.put("a", 0);
		List<Byte> tmp = lempel.convertDictionaryToByte(dictionary);

		// + 2 for the size of the dictionary
		assertEquals(2 + dictionary.size() * 4, tmp.size());

		dictionary.put("b", 1);
		dictionary.put("c", 2);

		tmp = lempel.convertDictionaryToByte(dictionary);

		assertEquals(2 + dictionary.size() * 4, tmp.size());
	}

	@Test
	public void isDictionaryContentOK() {
		Map<String, Integer> dictionary = new HashMap<>();
		dictionary.put("a", 1);
		dictionary.put("b", 2);
		dictionary.put("c", 3);
		dictionary.put("ba", 4);

		List<Byte> converted = lempel.convertDictionaryToByte(dictionary);
		byte[] bytes = new byte[converted.size()];
		for (int i = 0; i < converted.size(); i++) {
			bytes[i] = converted.get(i);
		}

		Map<String, Integer> decodedDictionary = lempel.decodeDictionary(Arrays.copyOfRange(bytes, 2, bytes.length));

		assertEquals(decodedDictionary, dictionary);

	}

}
