package com.istic.m2.compactor.algo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.istic.m2.compactor.util.RandomGenerator;

public class HuffmanTest {
	
	private Huffman huffman;
	
	@BeforeEach
	public void setUp() {
		huffman= new Huffman();
	}
	
	@Test
	public void sizeOfCodeTableInByteisOk() {
		
		Map<Character, String> codeTable = new HashMap<>();
		
		codeTable.put('c', "00");
		
		assertEquals(2 + codeTable.size()*3, huffman.convertCodeTableToByte(codeTable).size());

		
		codeTable.put('a', "0101100");
		codeTable.put('d', "11");
		
		assertEquals(2 + codeTable.size()*3, huffman.convertCodeTableToByte(codeTable).size());
		
		codeTable.put('x',"0000055522");
		
		assertEquals(2 + 1 + codeTable.size()*3, huffman.convertCodeTableToByte(codeTable).size());
	}
	
	@Test
	public void decodeCodeTableIsOkForCodeInf8() {
		Map<Character, String> codeTable = new HashMap<>();
		
		codeTable.put('c', "00");		
		codeTable.put('a', "0101100");
		codeTable.put('d', "11");
		
		List<Byte> bytes = huffman.convertCodeTableToByte(codeTable);
		byte[] byteCode = new byte[bytes.size()];
		for(int i = 0; i<bytes.size();i++) {
			byteCode[i] = bytes.get(i);
		}
		Map<Character, String> decodedCodeTable = null;
		try {
			decodedCodeTable = huffman.decodeCodeTable(new InputStream() {
				private int index =0;
				@Override
				public int read() throws IOException {
					
					return byteCode[index++];
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		assertEquals(codeTable, decodedCodeTable);
	}
	
	@Test
	public void decodeCodeTableIsOkForCodeInf16() {
		Map<Character, String> codeTable = new HashMap<>();
		
		codeTable.put('c', "00");		
		codeTable.put('a', "0101100");
		codeTable.put('d', "11");
		codeTable.put('z', "01010101110100");
		codeTable.put('x', "11111");
		
		List<Byte> bytes = huffman.convertCodeTableToByte(codeTable);
		byte[] byteCode = new byte[bytes.size()];
		for(int i = 0; i<bytes.size();i++) {
			byteCode[i] = bytes.get(i);
		}
		Map<Character, String> decodedCodeTable = null;
		try {
			decodedCodeTable = huffman.decodeCodeTable(new InputStream() {
				private int index =0;
				@Override
				public int read() throws IOException {
					
					return byteCode[index++];
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(codeTable, decodedCodeTable);
	}
	
	
	
	@Test
	public void createCodeTableHasCorrectSize() {
		String input = "blablabla";
		
		Huffman.Node root = null;
		try {
			root = huffman.buildTree(new ByteArrayInputStream(input.getBytes()));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		Map<Character,String> codeTable = new HashMap<>();
		Huffman.encode(root,"",codeTable);
		
		assertEquals(3, codeTable.size());
		
		input = "blablablazzzz";
		try {
			root = huffman.buildTree(new ByteArrayInputStream(input.getBytes()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		codeTable = new HashMap<>();
		Huffman.encode(root,"",codeTable);
		
		
		assertEquals(4, codeTable.size());
	}
	
	@Test
	public void correctlyEncode() {
		String input = "blablablazzz";
		Map<Character, String> codeTable = new HashMap<>();
		
		codeTable.put('a', "00");
		codeTable.put('b', "011");
		codeTable.put('l', "101");
		codeTable.put('z', "010");
		
		
		
	}
	
	@Test
	public void compressToUncompressed() throws SecurityException, IOException {
		Logger LOGGER = Logger.getLogger("HuffLogger");
		FileHandler handler = new FileHandler("compressToUncompressed.log", true);
		LOGGER.addHandler(handler);

		String input = RandomGenerator.generateRandomAlphanumericString();

		for (int i = 0; i < 1000; i++) {
			ByteArrayOutputStream tmp = new ByteArrayOutputStream();
			huffman.compress(new ByteArrayInputStream(input.getBytes()), tmp);
			tmp.close();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			huffman.expand(new ByteArrayInputStream(tmp.toByteArray()), out);
			String output = new String(out.toByteArray());
			if (!input.equals(output)) {
				LOGGER.severe(Instant.now() + " : Input = " + input
						+ "\nOutput =" + output);
				fail("Input and output are different, see default.log");
			}
			tmp.flush();
			out.flush();
			
			input = RandomGenerator.generateRandomAlphanumericString();

		}
	}
}
