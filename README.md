

# Compactor Project
**usage**: inputFile outputFile [options]

- -h : get help
- -d : decompress file
- -c : compress file
- -a or --algorithm choose the algorithm between Huffman and LZW

uses the Huffman algorithm as default 

**Error codes:**

1. Incorrect Algorithm
2. You cannot compress and decompress at the same time
3. File read/write exception
4. code table entry exceed max dimension (there will be data loss)
5. code table length exceed maximum length (there will be data loss)

## Development notes:
We have choosen java as a programming language beacause it is the language we know the most.
But as we began to implement the algorithms we learned that you canot directly read or write bit by bit in java, you have to do it byte by byte and shift the bytes bit by bit.
There is also no unsigned integer type in java other than char. So if a byte with a value greater than 127 will be read as a negative number. This complexifies greatly the algorithm's implementation and we think that java was not the best programming language choice.

We used maven as a dependancy manager. This also eases CI integration. We used picocli to easily make a command line interface. 

We use a code table that we put at the begining of the newly compressed file. So if the input file is made of a few bytes, the compressed file will be bigger thant the uncompressed one.

## Implemented Algorithms

In the first hours of the project we both worked on the Huffman algorithm and then splitted with Etienne Imbert working on huffman and Brian Le Garrec working on LZW.

### Huffman

The file compressed output file format for huffman algorithm is the folowing : 
![alt text](reportImages/huffmanCodeTable.png)

The compressed file code is then put after the code table.

### LZW

We tried to implement LZW and made test cases for it. But we lost time trying to understand the algorithm and didn't realy know how to implement it. Currently the algorithm basis is in place but we have some limit cases not working. The second algorithm status is currently in development.

##  Testing and CI usage

### CI
For each pushed commit the CI integration will run `mvn -B verify`
This will trigger :
 - the unit test using Junit Jupiter and maven surfire
 - the coverage test using jacoco
 
 A coverage of less than 60% of instructions on a file will make the pipeline fail.

### Testing

We used some samples of our own to try and better understand the algorithms and see by ourselves where the bugs happened. 

We also use `xxd -b <file>` to see what was written by our compression algorithm and try to identify.

After implementing the code table methods on huffman we have made the unit tests corresponding to them to ensure that further developments won't break this code and if so identify and fix it rapidly.
We then implemented the whole algorithm, made test cases for it and modified the algorithm to work with the tests.

### Fuzzing

We had originally planned to use a fuzzing library/framework but after examination they would have taken too long or been too much of a hastle to add to our application, so we decided to instead create a RandomGenerator utility class.
It allows us to randomly generate integers and strings to use in our test cases. For now we have only used random strings in the compressToUncompressed test case of HuffmanTest.

For instance with fuzzing we identified a bug where we didn't read from the correct point in the file when decoding.


## ISSUES Encountered

We have encountered issues with correctly writing bytes in java (to read bits we need to use >> and << operators)

We have encountered issues with decoding the code table with huffman algorithms (sometimes the code for a byte was more than one byte)

With Huffman we had trouble with decoding there was one byte that was not decoded correctly in our test cases.
 
We also needed to switch from byte array reading to inputStream reading as it is a better way of doing when dealing with large files.
this implied bugs on huffman encode re-do, unit tests and debug mode pointed out that the code table was well encoded/decoded. The bug was in the write method. 

We have noticed when implementing fuzzing that we are not able to decompress a file that was only 1 byte before compression.
The code table being unable to decode a code of size 0 (since there is only one code this code translates in nothing)
